---
date: 2022-05-14T21:07:13+01:00
title: Τεχνικό Φυλλάδιο UniverSIS
type: index
weight: 0
---

### Πληροφοριακό Σύστημα Ηλεκτρονικής Γραμματείας (Student Information System) για τη διαχείριση των ακαδημαϊκών θεμάτων των Πανεπιστημίων

Ο ιστοχώρος αυτός αποτελεί το τεχνικό φυλλάδιο του συστήματος και περιγράφει σε βάθος

* Το [UniverSIS]({{< ref "/intro" >}}) project
* Τις [λειτουργίες]({{< ref "/features" >}}) που προσφέρει μέσα από τις [εφαρμογές]({{< ref "/apps" >}}) του
* Τις [τεχνολογίες]({{< ref "/technology" >}}) που χρησιμοποιεί
* Τα επιπλέον [χαρακτηριστικά]({{< ref "/extras" >}}) του

Η ομάδα έργου της λύσης UniverSIS συντηρεί και ενημερώνει υλικό στα παρακάτω sites

* Ιστοχώρος του έργου [https://www.universis.gr](https://www.universis.gr)
* Ιστοχώρος της δράσης ανάπτυξης [https://www.universis.io](https://www.universis.io)
* Αποθετήρια κώδικα [https://gitlab.com/universis](https://gitlab.com/universis)


