---
title: "Εφαρμογές"
date: 2022-05-15T02:05:47+03:00
draft: false
---

## Φοιτητή - students {#students_app}

Μέσω της εφαρμογής ο φοιτητής (Φ) μπορεί να:

- Εγγραφεί σε διδακτικό εξάμηνο
- Υποβάλει **δηλώσεις μαθημάτων** και να επιλέξει από τμήματα τάξης του κάθε μαθήματος
- Επιλέξει **κατεύθυνση και ομάδες μαθημάτων** στο πρόγραμμα σπουδών του
- Βλέπει τα τρέχοντα δηλωμένα μαθήματα με τις σχετικές πληροφορίες ιστοσελίδας μαθήματος και elearning URL καθώς και ιστορικό δηλωμένων μαθημάτων και συμμετοχής σε εξετάσεις
- Βλέπει τα συγγράμματα που έχει δηλώσει στο εθνικό σύστημα Διανομής Συγγραμμάτων ΕΥΔΟΞΟΣ που έχει τη δυνατότητα να ελέγξει τη συμφωνία με τη δήλωση μαθημάτων του
- Ενημερώνεται για το προσωπικό του **πρόγραμμα μαθημάτων και εξετάσεων** σύμφωνα με τη δήλωση μαθημάτων που έχει κάνει
- Υποβάλει **δηλώσεις πρόθεσης συμμετοχής** σε εξεταστική μαθήματος και αποδοχής όρων για εξ αποστάσεως εξέταση μέσω ηλεκτρονικών εργαλείων
- Παρακολουθεί τις **πτυχιακές ή διπλωματικές εργασίες** που έχει αναλάβει προσωπικά ή ομαδικά και να ενημερώνεται για τον επιβλέποντα και τα μέλη της επιτροπής
- Ενημερώνεται για τους πρόσφατους βαθμούς και την θέση του στην κατανομή της καμπύλης βαθμολογίας
- Καταθέτει αίτημα αναβαθμολόγησης σε πρόσφατο βαθμό εξέτασής του
- Παρακολουθεί την πλήρη αναλυτική **βαθμολογία** του με μέσους όρους, μονάδες ECTS, απαλλασσόμενα μαθήματα και υπολογιζόμενα και μη στο βαθμό πτυχίου. Εκτυπώνει πρόχειρη έκδοση για ανεπίσημη χρήση
- Καταθέτει **αξιολόγηση** του μαθήματος και του κάθε διδάσκοντα στο μάθημα που ολοκληρώνει
- Εμφανίσει το προσωπικό προφίλ του στην Γραμματεία Τμήματος με στοιχεία προσωπικά και φοίτησης και δυνατότητα να επεξεργασίας κάποιων από αυτά που επιτρέπει το Τμήμα
- Καταθέσει **αιτήσεις** προς τη Γραμματεία Τμήματος για έκδοση πιστοποιητικών, ως ενεργός φοιτητής, επί πτυχίω και απόφοιτος.
- Καταθέσει αιτήσεις για **μεταβολή κατάστασης** σπουδών
- Πληρώνει με **ηλεκτρονική τραπεζική** για αιτήσεις ή πιστοποιητικά που τυχόν το απαιτούν
- Παραλάβει **πιστοποιητικά** σε ηλεκτρονική μορφή με ψηφιακές υπογραφές και σε εκτυπώσιμη μορφή με έλεγχο γνησιότητας
- Βλέπει ανακοινώσεις και να λαμβάνει **μηνύματα από την Γραμματεία** του Τμήματός του και από το σύστημα
- Λαμβάνει **μηνύματα από τους διδάσκοντες** των μαθημάτων του
- Βλέπει τον διδάσκοντα που έχει οριστεί από τη Γραμματεία ως **Ακαδημαϊκός του Σύμβουλος**
- Βλέπει τις **προϋποθέσεις λήψης πτυχίου** του και τον βαθμό κάλυψή τους
- Καταθέσει **αίτηση ορκωμοσίας** με τα συνοδευόμενα δικαιολογητικά και να παραλάβει τα επίσημα πιστοποιητικά του τίτλου σπουδών
- Αλλάζει **γλώσσα στο περιβάλλον** της εφαρμογής και στα δεδομένα που εμφανίζονται με κύρια υποστήριξη τα ελληνικά και τα αγγλικά
- **Επιβεβαιώσει στοιχεία εγγραφής** του και να **καταθέσει δικαιολογητικά**, εφόσον είναι σε κατάσταση προεγγραφής, ώστε η Γραμματεία να οριστικοποιήσει την εγγραφή του
- Συμπληρώνει φόρμες **αξιολόγησης μαθήματος και διδάσκοντα**

## Διδάσκοντα - teacher {#teacher_app}

Μέσω της εφαρμογής ο διδάσκοντας (Δ) μπορεί να:

- Εμφανίζει **μαθήματα και τάξεις** για τις οποίες έχει ανάθεση καθηκόντων διδασκαλίας στο τρέχον εξάμηνο
- Αναζητά **φοιτητές τάξης** με φίλτρα λέξης κλειδιού, τμήματος προέλευσης, εξαμήνου
- Εξάγει καταστάσεις φοιτητών στα μαθήματά του σε αρχεία .xls και .csv
- Στέλνει **ειδοποιήσεις** στο σύνολο τάξης που διδάσκει ή μεμονωμένα σε συγκεκριμένους φοιτητές του
- Ενημερώνεται για το προσωπικό του **πρόγραμμα διδασκαλίας και εξετάσεων** των μαθημάτων ανάθεσης
- Καταθέτει **παρουσίες και απουσίες** φοιτητών σε τάξεις διδασκαλίας του
- Εξάγει καταστάσεις φοιτητών που έχουν καταθέσει **πρόθεση συμμετοχής σε εξέταση** του  σε αρχεία .xls και .csv
- Καταθέτει **βαθμολογία μαθήματος** με δυνατότητα ψηφιακής υπογραφής σε αρχεία .xls και .csv. Λαμβάνει αποδεικτικό υποβολής με κωδικό επαλήθευσης περιεχομένου υποβολής
- Ενημερώνεται για **αλλαγές σε καταχωρημένους βαθμούς** του από τη Γραμματεία ή συνδιδάσκοντες
- Λαμβάνει ενημέρωση για **αιτήσεις αναβαθμολόγησης** από φοιτητές του
- Εμφανίζει βαθμολογίες εξετάσεων που είναι εκκρεμείς / σε εξέλιξη
- Εμφανίζει στατιστικά και **βαθμολογική κατανομή** τάξεων
- Εμφανίζει **ιστορικό μαθημάτων, τάξεων και εξετάσεων** σε βάθος χρόνου
- Εμφανίζει **ανοιχτές εργασίες** που έχει αναθέσει σε φοιτητές ή συμμετέχει σε επιτροπή επίβλεψης
- Εμφανίζει ιστορικό ολοκληρωμένων εργασιών
- Αναζητά πληροφορίες για το **ακαδημαϊκό προφίλ φοιτητή** που έχει διδάξει σε κάποιο μάθημα ή έχει επιβλέψει εργασία του, ώστε να μπορεί να ετοιμάσει συστατική επιστολή
- Εμφανίζει φοιτητές για τους οποίους εκείνος έχει οριστεί ως Ακαδημαϊκός Σύμβουλος από τη Γραμματεία
- Εμφανίζει το **προσωπικό προφίλ** του στην Γραμματεία Τμήματος με στοιχεία προσωπικά και επικοινωνίας
- Ενημερώνεται για την αξιολόγηση του μαθήματος και του ίδιου από τους φοιτητές του
- Καταχωρεί πληροφορίες για τον τρόπο διδασκαλίας του μαθήματος
- Καταχωρεί πληροφορίες για το ακαδημαϊκό του προφίλ

## Γραμματείας Τμήματος - registrar {#registrar_app}

Μέσω της εφαρμογής ο υπάλληλος Γραμματείας (Γ) Τμήματος ή Διαχειριστής Μεταπτυχιακού Προγράμματος Σπουδών έχει τη δυνατότητα να:

- Εκτελεί βασικές λειτουργίες (εισαγωγή, επισκόπηση, ενημέρωση, αλλαγή κατάστασης) σε **βασικές οντότητες**
    * Φοιτητών,
    * Διδασκόντων,
    * Μαθημάτων, απλά και σύνθετα
    * Τάξεων, με ένα ή περισσότερα τμήματα
    * Εξετάσεων,
    * Προγραμμάτων σπουδών,
    * Κατευθύνσεων και Ομάδων μαθημάτων
    * Εργασιών,
    * Πρακτικών,
    * Υποτροφιών κλπ
- Εκτελεί **αναζητήσεις** σε παραπάνω οντότητες, εφαρμόζοντας φίλτρα ελεύθερου κειμένου ή κλειστών επιλογών σε πίνακες/λίστες
- **Αποθηκεύει τα φίλτρα των αναζητήσεων** για εύκολη μελλοντική εφαρμογή
- **Εκτελεί ενέργειες μαζικά** πάνω σε αποτελέσματα των παραπάνω αναζητήσεων σε επιλεγμένους φοιτητές, διδάσκοντες, μαθήματα, τάξεις κλπ
- **Διαμορφώνει προγράμματα σπουδών** με τα μαθήματα που περιλαμβάνουν, τους διδάσκοντες, τους κανόνες προϋποθέσεων λήψης μαθημάτων και τα προαπαιτούμενα λήψης πτυχίου
- Διαχειρίζεται φοιτητές, το **πρόγραμμα σπουδών** τους, την **κατεύθυνση** που επιλέγουν, τους προσωπικούς **κανόνες λήψης πτυχίου** επιπλέον των γενικών του προγράμματος σπουδών τους, μεταβολές της κατάστασης σπουδών και την μαζική εισαγωγή τους σε πρόγραμμα σπουδών από λίστα υποψηφίων φοιτητών
- Εκτελεί **μεταφορά παλαιών φοιτητών** από παλαιό Τμήμα/πρόγραμμα σπουδών σε νεότερο με αυτόματη δημιουργία του και αντιγραφή μαθημάτων τους
- Εκτελεί **αντιστοιχίσεις μαθημάτων** μεταξύ παλαιού και νέου προγράμματος σπουδών
- Διαχειρίζεται τις **δηλώσεις μαθημάτων** των φοιτητών με αποδοχή και τροποποίηση. Βλέπει τα σχετικά συγγράμματα που έχει δηλώσει ο φοιτητής στο εθνικό σύστημα Διανομής Συγγραμμάτων ΕΥΔΟΞΟΣ
- Εισαγωγή και αφαίρεση μαθημάτων μαζικά σε επιλεγμένους φοιτητές με κριτήρια αναζήτησης ή ελεύθερης επιλογής
- Διαχειρίζεται τις **δηλώσεις εγγραφής** των φοιτητών σε εξάμηνο
- Καταχωρεί το **πρόγραμμα διδασκαλίας μαθημάτων και εξετάσεων**
- Διαχειρίζεται τις **παρουσίες/απουσίες φοιτητών** που καταθέτουν οι διδάσκοντες
- Διαχειρίζεται τις **βαθμολογίες μαθημάτων** που καταθέτουν οι διδάσκοντες
- Διαχειρίζεται τις **αιτήσεις φοιτητών για έκδοση πιστοποιητικών** με βήματα έγκρισης, ψηφιακής υπογραφής, πρωτοκόλλησης, δημοσίευσης, και αρχειοθέτησης, μεμονωμένα ή μαζικά για την εξυπηρέτηση μεγάλου πλήθους αιτήσεων σε περιόδους εγγραφής ή ορκωμοσίας
- **Διαμορφώνει σύνολο από πρότυπα πιστοποιητικά** για όλους τους σκοπούς που προβλέπονται, με εφαρμογή κανόνων για τη διαθεσιμότητά τους ανάλογα με την κατάσταση και το επίπεδο σπουδών του φοιτητή. Τα πιστοποιητικά ενσωματώνουν αριθμό πρωτοκόλλου και μηχανισμούς για αυτόματο **έλεγχο γνησιότητας**, με χρήση **ψηφιακών υπογραφών** για ηλεκτρονική διακίνηση και με χρήση **κωδικού QR** για έντυπη διακίνηση
- Διαχειρίζεται τις αιτήσεις φοιτητών για **μεταβολή κατάστασης φοίτησης** σε περιπτώσεις αναστολής, διαγραφής και καταγράφει το ιστορικό τους
- Διαχειρίζεται **συμμετοχές φοιτητών σε ορκωμοσία** με έλεγχο προϋποθέσεων, δικαιολογητικών, έκδοση πιστοποιητικών και εκτέλεση εκδήλωσης με φυσική ή διαδικτυακή παρουσία
- **Στέλνει ειδοποιήσεις** σε φοιτητές, είτε ως μηνύματα εντός της πλατφόρμας, είτε ως email, είτε ως SMS
- **Εξάγει αναφορές και στατιστικά διαγράμματα** βασικών μετρικών του συστήματος Ηλεκτρονικής Γραμματείας (εξέλιξη κατάστασης φοίτησης ανά έτος εγγραφής, στατιστικά βάση παραμέτρου καρτέλας φοιτητή κλπ)
- Διαχειρίζεται εργασίες **έναρξης ακαδημαϊκής περιόδου**, αλλαγή εξαμήνου, άνοιγμα δηλώσεων, άνοιγμα εξετάσεων κ.λ.π.
- Καταχωρεί **πτυχιακές ή διπλωματικές εργασίες** φοιτητών, είτε προσωπικές είτε ομαδικές και αναθέτει την επίβλεψή τους σε διδάσκοντες, με κύριο εποπτεύοντα και άλλα μέλη επιτροπής
- Ορίζει διδάσκοντες ως **Ακαδημαϊκούς Συμβούλους** σε φοιτητές
- **Εισάγει λίστες φοιτητών από αρχείο** σε κατάσταση προεγγραφής και να τους επιτρέπει να επιβεβαιώσουν στοιχεία τους και να καταθέσουν δικαιολογητικά πριν την οριστική εγγραφή τους


## Διαχείρισης Χρηστών - userAdmin {#userAdmin_app}

Μέσω της εφαρμογής ο Διαχειριστής (ΔΧ) του συστήματος έχει τη δυνατότητα να:

- **Διαχειρίζεται όλους τους χρήστες** του συστήματος και για τις τρεις εφαρμογές, και να εφαρμόζει **πολιτικές ασφαλείας** για τη διάρκεια ισχύος του κωδικού πρόσβασης και τους κανόνες πολυπλοκότητας που πρέπει να ικανοποιούν
- Διαχειρίζεται και να **διαμορφώνει ρόλους** με επιλογή ανοιχτού συνδυασμού επιτρεπτών ενεργειών, ώστε να εξυπηρετεί ανάγκες είτε για διαβαθμισμένα επίπεδα πρόσβασης, είτε για ειδικούς ρόλους με εξειδικευμένες απαιτήσεις
- Αναζητά με φίλτρα την **καταγραφή ενεργειών**(audit log) της εφαρμογής και να διαμορφώνει δικαιώματα πρόσβασης σε αυτή για τους άλλους χρήστες
- **Επιβλέπει την καλή λειτουργία** του συστήματος με παρακολούθηση ενδείξεων κατάστασης των υποσυστημάτων και μετρικών απόδοσης, ταχύτητας και απόκρισης


